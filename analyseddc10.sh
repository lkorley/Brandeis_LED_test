
if [ $# -lt 3 ]
then
 echo "Missing arguments!!! To run:"
 echo "sh analyseddc10.sh [input_dir] [output_dir] [# files] <starting index>"
 return 
fi

indir=$1
outdir=$2
num=$3
init=0

if [ $# -eq 4 ]
then
 init=$4
 num=`expr ${init} + ${num}`
fi
for i in `seq ${init} ${num}`
do
    echo "starting File ${i}"
    ./DDC10_data_readout 8191 ${indir}/${i}.txt ${output_dir}/${i}.root
done
