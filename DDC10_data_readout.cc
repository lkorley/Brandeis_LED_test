/////////////////////////////////////////////////////////////////////////////////////////
// To compile : g++ -I /usr/common/usg/software/ROOT/5.34.20/include/ `root-config --cflags --libs` -o DDC10_data_readout DDC10_data_readout.cc
// To execute : ./DDC10_data_readout [number of samples][input file name] [output filename]
/* Revision log :
 *
	4/25/2018 RW : Code for reading scope's text file and do the pulse finding.



*/
/////////////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cstdio>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>   // for check directory
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <time.h>
#include <numeric>
#include <TMatrixDSymEigen.h>
//#include <libRATEvent.so>
#include <TVector3.h>
#include <TString.h>
#include <TLine.h>
#include <TFile.h>
#include <TTree.h>
#include <TNtuple.h>
#include <TH1F.h>
#include <TAxis.h>
#include <TKey.h>
#include <TF1.h>
#include <TChain.h>
#include <TProfile.h>
#include <TSystem.h>
#include <TH2F.h>
#include <TMath.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TVectorD.h>
#include <TMatrix.h>
#include <TH2Poly.h>
//#include <boost/date_time.hpp>
using namespace std;
ifstream fin;
//define pulse finding parameters
double pulseThresh =8.0 ;
double windowSize = 3.0;
double edgeThresh = 3.0;
double lookforward = 3.0;
//int number_of_samples = 256;

//vector<int> run_info;
vector<double> raw_waveform;
vector<double> amplitude;
vector<double> charge_v;
vector<double> startv;
vector<double> endv;
vector<double> start_pointv;
vector<double> end_pointv;
vector<double> baseline;
vector<double> baselinev;
vector<double> pulse_left_edge;
vector<double> pulse_right_edge;
vector<double> amplitude_position;
vector<double> pl;
vector<double> pr;
// Simpson Integral
double SimpsIntegral(const vector<double>& samples, double baseline,int start, int end){
    int len;
    double qsum = 0.0;
    if ((end - start) % 2 == 0){
    /* If there are an even number of samples, then there are an odd
    number of intervals; but Simpson's rule works only on an even
    number of intervals. Therefore we use Simpson's method on the
    all but the final sample, and integrate the last interval
    using the trapezoidal rule */
        len = end - start - 1;
        qsum += (samples[end-1] + samples[end-2] - 2*baseline)/2.0;
    }
    else
        len = end - start;

    double qsimps;
    qsimps = samples[start]-baseline;
    for (int i=start; i<start+len; i+=2)
        qsimps += (samples[i]-baseline)*4;
    for (int i=start+1; i<len+start-1; i+=2)
        qsimps += (samples[i]-baseline)*2;
    qsimps += samples[start+len-1]-baseline;
    qsimps /= 3.0;

    qsum += qsimps;
    return qsum;
}


// Pulse Finding
void extract_event(vector<double> &v, double b ,double rms,int nos){
    double pThresh = pulseThresh * rms * sqrt(windowSize);
    double eThresh = edgeThresh * rms * sqrt(windowSize);
    //cout<<" vector size is : "<<v.size()<<endl;
    //getchar();
    //Let's looking for the Pulses
    for (int i=0;i<nos;i++){
        double integral = SimpsIntegral(v,b,i,i+windowSize);
        int left = 0;
        int right = 0;
        int temp_peak = 0;
        double temp_startv = 0;
        double temp_endv = 0;
        if (integral > pThresh){
            //cout<<" This is sample : "<<i<<endl;
            left = i;
            integral = 1.0e9;
            while (integral > eThresh && left > windowSize){
                left --;
                integral = SimpsIntegral(v,b,left,left+windowSize);
                temp_startv = v[left];

            }

            integral = 1.0e9;
            right = i + windowSize;
            bool end=false;
            while (!end){
                while (integral > eThresh && right<9999){
                    right ++;
                    integral = SimpsIntegral(v,b,right-windowSize,right);
                    temp_endv = v[right];

                }
                end = true ;
                int r = right;
                while (r < fmin((int)v.size()-1, right+lookforward)){
                    r++ ;
                    integral = SimpsIntegral(v,b,r-windowSize,r);
                    if (integral > pThresh){
                        right = r;
                        end = false;
                        break;
                    }
                }
            }

            double max = -1.0e9;
            for (int j=left;j<right;j++){
                double s = v[j] - b;
                if (s > max){
                    max = s;
                    temp_peak = j;
                }
            }
            if (right >nos)
                continue;
            //cout<<" Peak is : "<<temp_peak<<" max is : "<<max<<endl;
            amplitude.push_back(max);
            amplitude_position.push_back(temp_peak);
            pulse_left_edge.push_back(left);
            pulse_right_edge.push_back(right);
            pl.push_back(left);
            pr.push_back(right);
            charge_v.push_back(SimpsIntegral(v,b,left,right));
            startv.push_back(temp_startv);
            endv.push_back(temp_endv);
            if (SimpsIntegral(v,b,left,right) <= eThresh){
                i = right -1;
                continue;
            }
            i = right;
        }//if statement

    }
    //getchar();
}
// Find the baseline
double baseline_rms(vector<double> &v, vector<double> &sample, int nosamples){
    double rms=0;
    double baseline_samples = accumulate(v.begin(),v.end(),0);
    baseline_samples /= v.size();
    for (int i=0;i<v.size();i++){
        rms += pow(v[i] - baseline_samples,2);
    }
    rms = sqrt(rms / v.size());
    //cout<<" rms is : "<<rms<<endl;
    extract_event(sample,baseline_samples,rms,nosamples);
    return rms;
}
int main(int argc, char *arg[]){
    int number_of_samples;
    number_of_samples = atoi (arg[1]);
    string filename;
    string outfilename;
    filename = arg[2];
    outfilename = arg[3];
    int j=0;
    double temp_sum=0;
    // Plots for debugging pulse finding algorithm
    TGraph* t11;
    TGraph* t22;
    TGraph* t33;
    TGraph* t44;

    char linea[200];
    TString* buff=new TString();
    char open_filename[50];
    int event=0;
    int iana = 0;
    int pcount=0;
    int header=0;
    int sweep=0;
    double rms_value;
    sprintf(open_filename,"%s",filename.c_str());
    cout<<" We are opening "<<open_filename<<endl;
    cout<<" Now processing event "<<event<<endl;

    TFile* fout = new TFile(outfilename.c_str(),"RECREATE");

    TH1D* h = new TH1D(("ADC_sum_waveform" + filename).c_str(),
                     ("#font[132]{WFD "+filename+" SumWaveForm}").c_str(),
                           10000, 0, 10000);
    h->SetXTitle("#font[132]{Sample (2ns)}");
    h->GetXaxis()->SetLabelFont(132);
    h->GetYaxis()->SetLabelFont(132);



    //vector< vector<double> > adc_sum_h;
    TNtuple *pulse = new TNtuple("pulse","pulse","pulseHeight:pulseRightEdge:pulseLeftEdge:pulseCharge:pulsePeakTime");

    TCanvas *waveplot[100];
    //reading data
    fin.open(open_filename,ios::in);
    if (!fin.is_open()){
        cout<<" Can't open the file "<<endl;
    }
    else{
        while(!fin.rdstate() ){
        //while(sweep < 682){
            fin.getline(linea,200);//>>datum;
            //cout<<" each line is : "<<linea;
    		*buff=linea;


            if (buff->Contains("Event")){

                if (sweep>0){
                    cout<<" this is sweep : "<<sweep<<endl;
                    //rms_value = baseline_rms(baselinev);
                    //extract_event(raw_waveform,rms_value);
                    rms_value = baseline_rms(baselinev,raw_waveform,number_of_samples);
                    //getchar();

                    //t22->SetPoint(iana,start_pointv[iana],startv[iana]);
                    //t33->SetPoint(iana,end_pointv[iana],endv[iana]);
                    if (sweep < 100){
                        for (int j=0;j<pulse_left_edge.size();j++){
                            t22->SetPoint(j,pulse_left_edge[j],startv[j]);
                            t33->SetPoint(j,pulse_right_edge[j],endv[j]);
                        }

                        char plotname [30];
                        sprintf(plotname,"waveform%d",iana);
                        waveplot[iana] = new TCanvas(plotname);
                        TLine* line = new TLine(0,rms_value,10000,rms_value);
                        line->SetLineColor(3);
                        line->SetLineStyle(3);
                        line->SetLineWidth(3);
                        t22->SetMarkerColor(2);
                        t22->SetMarkerStyle(3);
                        t22->SetMarkerSize(3);
                        t33->SetMarkerColor(4);
                        t33->SetMarkerStyle(3);
                        t33->SetMarkerSize(3);
                        t11->Draw("alp");
                        t22->Draw("p");
                        t33->Draw("p");
                        line->Draw("");
                        waveplot[iana]->Write();
                    }

                    cout<<" This is sweep : "<<sweep<<endl;
                    iana++;
                    pulse_left_edge.clear();
                    pulse_right_edge.clear();
                    startv.clear();
                    endv.clear();
                }

                raw_waveform.clear();
                if (sweep < 100){
                    //cout<<" Create new TGraph ! "<<endl;
                    t11 = new TGraph();
                    t22 = new TGraph();
                    t33 = new TGraph();
                }
                pcount = 0;
                sweep ++;
            }
            //cout<<"pcount is : "<<pcount<<endl;



            else{
                double datum = buff->Atof();
                if (pcount<number_of_samples+1){
                    datum*=-1;
                    cout<<" This is sample "<<pcount<<" with value : "<<datum<<endl;
                    if (isinf(datum) || isnan(datum) || datum > 200000 || datum<-1){
                        datum=0;
                    }
                    if (pcount<100)
                        baselinev.push_back(datum);
                    cout<<" Datum is : "<<datum<<endl;
                    raw_waveform.push_back(datum);

                    //adc_sum_h[iana].push_back(datum);
                    //cout<<" Drawing plot "<<endl;
                    //cout<<" pcount is : "<<pcount<<" datum is : "<<datum<<endl;
                    t11->SetPoint(pcount,pcount,datum);
                    //cout<<" Reading data , pcount is : "<<pcount<<endl;
                    //cout<<" Get sum_waveform BinContent !"<<endl;
                    temp_sum = h->GetBinContent(pcount+1);
                    //cout<<" Set sum_waveform BinContent !"<<endl;
                    h->SetBinContent(pcount+1,temp_sum+datum);

                }
                pcount++;

            }




        }
    }

    //Fill Ntuple
    //pulseHeight:pulseRightEdge:pulseLeftEdge:pulseCharge:pulsePeakTime
    for (int i=0;i<amplitude.size();i++){

        pulse->Fill(amplitude[i],pr[i],pl[i],charge_v[i],amplitude_position[i]);
    }

    cout<<" Total sweeps is : "<<sweep<<endl;




    pulse->Write();
    fout->Write();
    fout->Close();

    return 0;
}
